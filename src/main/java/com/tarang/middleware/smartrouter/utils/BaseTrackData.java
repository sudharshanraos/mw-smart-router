
package com.tarang.middleware.smartrouter.utils;

/**
 *  
 * @author aruna

 * @version 1.0
 * @description This class parse the Track data
 * 
 */
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.io.Serializable;
import java.util.regex.Matcher;

abstract class BaseTrackData implements RawData, Serializable {

	private static final long serialVersionUID = 7821463290736676016L;

	protected static String getGroup(final Matcher matcher, final int group) {
		final int groupCount = matcher.groupCount();
		if (groupCount > group - 1) {
			return matcher.group(group);
		} else {
			return null;
		}
	}

	private final String rawTrackData;
	private final String discretionaryData;

	BaseTrackData(final String rawTrackData, final String discretionaryData) {
		this.rawTrackData = rawTrackData;
		this.discretionaryData = discretionaryData == null ? "" : discretionaryData;
	}

	/**
	 * Gets discretionary data on the track.
	 *
	 * @return Discretionary data.
	 */
	public String getDiscretionaryData() {
		return discretionaryData;
	}

	@Override
	public String getRawData() {
		return rawTrackData;
	}

	/**
	 * Whether discretionary data is present.
	 *
	 * @return True if discretionary data is available
	 */
	public boolean hasDiscretionaryData() {
		return !isBlank(discretionaryData);
	}

	@Override
	public boolean hasRawData() {
		return !isBlank(rawTrackData);
	}

	@Override
	public String toString() {
		return rawTrackData;
	}

}
