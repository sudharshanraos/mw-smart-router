package com.tarang.middleware.smartrouter.utils;

/**
 * 
 * @author aruna
 * @version 1.0
 * @description This class having Raw Data for Tracks
 * 
 */
public interface RawData {

	/**
	 * Whether the raw data exceeds the maximum length allowed.
	 *
	 * @return True if too long
	 */
	boolean exceedsMaximumLength();

	/**
	 * Raw data.
	 *
	 * @return Raw data
	 */
	String getRawData();

	/**
	 * Whether raw data is present.
	 *
	 * @return True if raw data is available.
	 */
	boolean hasRawData();

}
