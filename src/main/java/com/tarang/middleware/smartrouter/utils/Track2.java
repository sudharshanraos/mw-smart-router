
package com.tarang.middleware.smartrouter.utils;

import static org.apache.commons.lang3.StringUtils.trimToEmpty;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Track2 extends BaseBankCardTrackData {

	private static final long serialVersionUID = 1L;

	private static final Pattern track2Pattern = Pattern
			.compile(".*[\\t\\n\\r ]?(;([0-9]{1,19})=([0-9]{4})([0-9]{3})(.*)\\?).*");

	/**
	 * Parses magnetic track 2 data into a Track2 object.
	 *
	 * @param rawTrackData
	 *            Raw track data as a string. Can include newlines, and other tracks
	 *            as well.
	 * @return A Track2 instance, corresponding to the parsed data.
	 */
	public static Track2 from(final String rawTrackData) {
		final Matcher matcher = track2Pattern.matcher(trimToEmpty(rawTrackData));

		final String rawTrack2Data;
		final String pan;
		final String expirationDate;
		final String serviceCode;
		final String discretionaryData;

		if (matcher.matches()) {
			rawTrack2Data = getGroup(matcher, 1);
			pan = getGroup(matcher, 2);
			expirationDate = getGroup(matcher, 3);
			serviceCode = getGroup(matcher, 4);
			discretionaryData = getGroup(matcher, 5);
		} else {
			rawTrack2Data = null;
			pan = new String();
			expirationDate = new String();
			serviceCode = new String();
			discretionaryData = "";
		}

		return new Track2(rawTrack2Data, pan, expirationDate, serviceCode, discretionaryData);
	}

	private Track2(final String rawTrackData, final String pan, final String expirationDate, final String serviceCode,
			final String discretionaryData) {
		super(rawTrackData, pan, expirationDate, serviceCode, discretionaryData);
	}

	@Override
	public boolean exceedsMaximumLength() {
		return hasRawData() && getRawData().length() > 40;
	}

}
