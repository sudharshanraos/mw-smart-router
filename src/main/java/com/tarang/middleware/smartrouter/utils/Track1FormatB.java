package com.tarang.middleware.smartrouter.utils;

/**
 * 
 * @author aruna

 * @version 1.0
 * @description This class used for parsing the Magnetic strip  data
 * 
 */

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Track1FormatB extends BaseBankCardTrackData {

	private static final long serialVersionUID = 3020739300944280022L;

	private static final Pattern track1FormatBPattern = Pattern.compile(
			"(%?([A-Z])([0-9]{1,19})\\^([^\\^]{2,26})\\^([0-9]{4}|\\^)([0-9]{3}|\\^)?([^\\?]+)?\\??)[\t\n\r ]{0,2}.*");

	/**
	 * Parses magnetic track 1 format B data into a Track1FormatB object.
	 *
	 * @param rawTrackData
	 *            Raw track data as a string. Can include newlines, and other
	 *            tracks as well.
	 * @return A Track1FormatB instance, corresponding to the parsed data.
	 */
	public static Track1FormatB from(final String rawTrackData) {
		final Matcher matcher = track1FormatBPattern.matcher(trimToEmpty(rawTrackData));

		final String rawTrack1Data;
		final String pan;
		final String expirationDate;
		final String name;
		final String serviceCode;
		final String formatCode;
		final String discretionaryData;

		if (matcher.matches()) {
			rawTrack1Data = getGroup(matcher, 1);
			formatCode = getGroup(matcher, 2);
			pan = getGroup(matcher, 3);
			name = getGroup(matcher, 4);
			expirationDate = getGroup(matcher, 5);
			serviceCode = getGroup(matcher, 6);
			discretionaryData = getGroup(matcher, 7);
		} else {
			rawTrack1Data = null;
			formatCode = "";
			pan = new String();
			name = new String();
			expirationDate = new String();
			serviceCode = new String();
			discretionaryData = "";
		}

		return new Track1FormatB(rawTrack1Data, pan, expirationDate, name, serviceCode, formatCode, discretionaryData);
	}

	private final String name;
	private final String formatCode;

	private Track1FormatB(final String rawTrackData, final String pan, final String expirationDate, final String name,
			final String serviceCode, final String formatCode, final String discretionaryData) {
		super(rawTrackData, pan, expirationDate, serviceCode, discretionaryData);
		this.formatCode = formatCode;
		this.name = name;
	}

	@Override
	public boolean exceedsMaximumLength() {
		return hasRawData() && getRawData().length() > 79;
	}

	/**
	 * Gets the track 1 format code, usually "B".
	 *
	 * @return Track 1 format code, usually "B"
	 */
	public String getFormatCode() {
		return formatCode;
	}

	/**
	 * Gets the cardholder's name.
	 *
	 * @return Cardholder's name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Checks whether the format code is available.
	 *
	 * @return True if the format code is available.
	 */
	public boolean hasFormatCode() {
		return !isBlank(formatCode);
	}

	/**
	 * Checks whether the cardholder's name is available.
	 *
	 * @return True if the cardholder's name is available.
	 */
	public boolean hasName() {
		return name != null;
	}

}
