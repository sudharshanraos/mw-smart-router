package com.tarang.middleware.smartrouter.utils;

/**
 *  
 * @author aruna
 * @version 1.0
 * @description This class parse the Track data
 * 
 */

abstract class BaseBankCardTrackData extends BaseTrackData {

	private static final long serialVersionUID = 7821463290736676016L;

	private final String pan;
	private final String expirationDate;
	private final String serviceCode;

	BaseBankCardTrackData(final String rawTrackData, final String pan, final String expirationDate,
			final String serviceCode, final String discretionaryData) {
		super(rawTrackData, discretionaryData);
		this.pan = pan;
		this.expirationDate = expirationDate;
		this.serviceCode = serviceCode;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof BaseBankCardTrackData)) {
			return false;
		}
		final BaseBankCardTrackData other = (BaseBankCardTrackData) obj;
		if (expirationDate == null) {
			if (other.expirationDate != null) {
				return false;
			}
		} else if (!expirationDate.equals(other.expirationDate)) {
			return false;
		}
		if (pan == null) {
			if (other.pan != null) {
				return false;
			}
		} else if (!pan.equals(other.pan)) {
			return false;
		}
		if (serviceCode == null) {
			if (other.serviceCode != null) {
				return false;
			}
		} else if (!serviceCode.equals(other.serviceCode)) {
			return false;
		}
		return true;
	}

	/**
	 * Gets the primary account number for the card.
	 *
	 * @return Primary account number.
	 */
	public String getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Gets the primary account number for the card.
	 *
	 * @return Primary account number.
	 */
	public String getPrimaryAccountNumber() {
		return pan;
	}

	/**
	 * Gets the card service code.
	 *
	 * @return Card service code.
	 */
	public String getServiceCode() {
		return serviceCode;
	}

	/**
	 * Checks whether the card expiration date is available.
	 *
	 * @return True if the card expiration date is available.
	 */
	public boolean hasExpirationDate() {
		return expirationDate != null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (expirationDate == null ? 0 : expirationDate.hashCode());
		result = prime * result + (pan == null ? 0 : pan.hashCode());
		result = prime * result + (serviceCode == null ? 0 : serviceCode.hashCode());
		return result;
	}

	/**
	 * Checks whether the primary account number for the card is available.
	 *
	 * @return True if the primary account number for the card is available.
	 */
	public boolean hasPrimaryAccountNumber() {
		return pan != null;
	}

	/**
	 * Checks whether the card service code is available.
	 *
	 * @return True if the card service code is available.
	 */
	public boolean hasServiceCode() {
		return serviceCode != null;
	}

	/**
	 * Verifies that the available data is consistent between Track 1 and Track
	 * 2, or any other track.
	 *
	 * @return True if the data is consistent.
	 */
	public boolean isConsistentWith(final BaseBankCardTrackData other) {
		if (this == other) {
			return true;
		}
		if (other == null) {
			return false;
		}
		boolean equals = true;

		if (hasPrimaryAccountNumber() && other.hasPrimaryAccountNumber()) {
			if (!getPrimaryAccountNumber().equals(other.getPrimaryAccountNumber())) {
				equals = false;
			}
		}

		if (hasExpirationDate() && other.hasExpirationDate()) {
			if (!getExpirationDate().equals(other.getExpirationDate())) {
				equals = false;
			}
		}

		if (hasServiceCode() && other.hasServiceCode()) {
			if (!getServiceCode().equals(other.getServiceCode())) {
				equals = false;
			}
		}

		return equals;
	}

}
