package com.tarang.middleware.smartrouter.constants;

/**
 * 
 * @author aruna
 * @version 1.0
 * @description This class having POS Fields
 * 
 */
public final class PosFields {

	public static final int MTI = 0;
	public static final int PROCESSING_CODE = 3;
	public static final int SALE_AMOUNT = 4;
	public static final int TRANSACTION_DATE_TIME = 7;
	public static final int STAN = 11;
	public static final int posMode = 22;
	public static final int USER_ID = 32;
	public static final int RRN_FIELD = 37;
	public static final int TERMINAL_ID = 41;
	public static final int MERCHANT_ID = 42;
	public static final int TRACK_1 = 45;
	public static final int CURRENCY = 49;
	public static final int PIN_DATA = 52;
	public static final int PRIVATE_DATA1 = 63;
	public static final int TRACK_2 = 35;
	public static final int FI_ID = 43;
	public static final int RECEIPT_MESSAGE = 44;
	public static final int PIN = 52;
	public static final int BALANCE = 54;
	public static final int ORIGINAL_TRANSACTION_ID = 75;
	public static final int ORIGINAL_TRANSACTION_AMOUNT = 77;
	public static final int ENCRYPTED_PIN = 112;
	public static final int TAX_AMOUNT = 112;
	public static final int REPORT_PARAM = 115;
	public static final int REPORT_CONTENT = 116;
	public static final int REWARDS_CONTENT = 116;
	public static final int REWARDS_PARAM = 116;
	public static final int CONFIRMED_NEW_PIN = 116;
	public static final int NEW_PIN = 117;
	public static final int FEE_AMOUNT = 117;
	public static final int RECEIVED_AMOUNT = 118;
	public static final int PERSONAL_INFO = 121;
	public static final int CASHIER_CODE = 124;

}