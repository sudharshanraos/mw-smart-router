package com.tarang.middleware.smartrouter.constants;

/**
 * The Interface ApiConstants.
 * 
 * @author sudharshan.s
 */
public final class ApiConstants {

    /** The Constant PARENT. */
    public static final String PARENT = "/";

    /** The Constant PAYMENT_SERVICE_MANAGEMENT. */
    public static final String PAYMENT_SERVICE_MANAGEMENT = "Payment Service Management";
    
    /** The Constant SYSTEM_CONFIG_MANAGEMENT. */
    public static final String SYSTEM_CONFIG_MANAGEMENT = "System Configuration Management";
    
    /** The Constant PS_MADA_PARENT. */
    public static final String PS_MADA_PARENT = "/mada";

    /** The Constant PS_MADA_AUTH. */
    public static final String PS_MADA_AUTH = "/auth";
    
    /** The Constant PS_MADA_SALE. */
    public static final String PS_MADA_SALE = "/sale";
    
    /** The Constant PS_MADA_REFEND. */
    public static final String PS_MADA_REFEND = "/refund";

    /** The Constant SYSTEM_PARENT. */
    public static final String SYSTEM_PARENT = "/system";

    /** The Constant SYSTEM_GET_ENCRYPT_DATA. */
    public static final String SYSTEM_GET_ENCRYPT_DATA = "/encrypt";

    /** The Constant SYSTEM_GET_DECRYPT_DATA. */
    public static final String SYSTEM_GET_DECRYPT_DATA = "/decrypt";
    
    
    
    
    public static final String BASE_URL = "/";

    public static final String SOURCE_MSG_MAPPING = "SOURCE MESSAGE MAPPING";
    public static final String SOURCE_MSG_MAPPING_MANAGEMENT = "SOURCE MESSAGE MAPPING MANAGEMENT";
    public static final String BASE_CONTEXT = "/smartRouter";
    public static final String GET_SOURCE_MSG_MAPPING = "/GetSourceMsgMapping";
    
    public static final String QUESTION = "?";
    public static final String PARAM_PORT = "port";
    public static final String EQUAL = "=";
    public static final String TARGET_NAME = "targetName";
    
    /** The Constant PAYMENT_SERVICE_MANAGEMENT. */
    public static final String SOFT_POS_SERVICE_MANAGEMENT = "Soft POS Service Management";

    /** The Constant PS_MADA_AUTH. */
    public static final String SOFT_POS_MADA_AUTH = "/auth";
    /**
     * Instantiates a new api constants.
     */
    private ApiConstants() {
        // private constructor
    }

}
