package com.tarang.middleware.smartrouter;

import java.net.InetSocketAddress;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.tarang.middleware.smartrouter.service.io.PaymentServiceChannelInitializer;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

@Service("isoMessagesServer")
public class PosMessageServer {

    @Value("${netty.tcp-port}")
    private int tcpPort;
    
    EventLoopGroup bossGroup = new NioEventLoopGroup(1);
    EventLoopGroup workerGroup = new NioEventLoopGroup();

    
    @Autowired
    private PaymentServiceChannelInitializer psChannelInitializer;
    public void start() {

        try {
            ServerBootstrap bootStrap = new ServerBootstrap();
            bootStrap.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO)).childHandler(psChannelInitializer);

            ChannelFuture channelFuture = bootStrap.bind(new InetSocketAddress(tcpPort))
                    .sync();
            System.out.println(" PosHandler started on port " +tcpPort);
            channelFuture.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            //
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();

        }
        System.exit(0);
        
        
        
        
        
    }

    public void restart() {
        bossGroup.shutdownGracefully();
        this.start();
    }

    public void stop() {
        bossGroup.shutdownGracefully();
    }

}
