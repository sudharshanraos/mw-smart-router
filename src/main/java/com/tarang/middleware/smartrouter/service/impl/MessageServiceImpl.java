package com.tarang.middleware.smartrouter.service.impl;


import javax.annotation.PostConstruct;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tarang.middleware.smartrouter.constants.ApiConstants;
import com.tarang.middleware.smartrouter.dto.SourceMessageConfigResponse;
import com.tarang.middleware.smartrouter.dto.TargetConfigDto;
import com.tarang.middleware.smartrouter.service.MessageService;

@Service
public class MessageServiceImpl implements MessageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageServiceImpl.class);
    
    
    private HttpHeaders sourceHeaders;
    
    @Value("${config.server.host.url}")
    private String configServerHostUrl;
    
    @Value("${target.server.host.url}")
    private String targetServerHostUrl;
    
    @Value("${config.server.sourceMsgDetails.url}")
    private String configServerSourceMsgDetailstUrl;
    
    @Value("${config.server.targetConfigDetails.url}")
    private String configServerTargetConfigDetailsUrl;
    
    
    @PostConstruct
    public void postConstruct() {

        sourceHeaders = new HttpHeaders();
        sourceHeaders.setContentType(MediaType.APPLICATION_JSON);
        
    }
    
  
    @Override
    public SourceMessageConfigResponse getSourceMsgMapping(String port) throws Exception {
        
        ResponseEntity<String> responseEntity = null;
        RestTemplate restTemplate = new RestTemplate();
        StringBuilder serviceUrl = new StringBuilder(); 
        serviceUrl.append(configServerHostUrl);
        serviceUrl.append(configServerSourceMsgDetailstUrl);
        serviceUrl.append(ApiConstants.QUESTION);
        serviceUrl.append(ApiConstants.PARAM_PORT);
        serviceUrl.append(ApiConstants.EQUAL);
        serviceUrl.append(port);
        SourceMessageConfigResponse msgResponse = new SourceMessageConfigResponse();
        
        responseEntity = restTemplate.getForEntity(serviceUrl.toString(), String.class);
        if (responseEntity != null && HttpStatus.OK.is2xxSuccessful()) {
            JSONObject responseData = new JSONObject(responseEntity.getBody());
            System.out.println("responseData---"+responseData);
            //msgResponse = responseData;
             //config = (SourceConfig) responseData.get("sourceObj");
            		
         // ObjectMapper instantiation
            ObjectMapper mapper = new ObjectMapper();

            // Deserialization into the `Employee` class
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
           // mapper.setVisibility(VisibilityChecker.Std.defaultInstance().withFieldVisibility(JsonAutoDetect.Visibility.ANY));
            mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
            msgResponse = mapper.readValue(responseData.toString(), SourceMessageConfigResponse.class);

            
        }
            
        LOGGER.debug("responseEntity StatusCode {0}", responseEntity.getStatusCodeValue());
        return msgResponse;    
        
    }


	@Override
	public TargetConfigDto getTargetConfigByTargetName(String targetName) throws Exception {
        ResponseEntity<String> responseEntity = null;
        RestTemplate restTemplate = new RestTemplate();
        StringBuilder serviceUrl = new StringBuilder(); 
        serviceUrl.append(configServerHostUrl);
        serviceUrl.append(configServerTargetConfigDetailsUrl);
        serviceUrl.append(ApiConstants.QUESTION);
        serviceUrl.append(ApiConstants.TARGET_NAME);
        serviceUrl.append(ApiConstants.EQUAL);
        serviceUrl.append(targetName);
        TargetConfigDto targetConfigDto = new TargetConfigDto();
        
        responseEntity = restTemplate.getForEntity(serviceUrl.toString(), String.class);
        if (responseEntity != null && HttpStatus.OK.is2xxSuccessful()) {
            JSONObject responseData = new JSONObject(responseEntity.getBody());
            System.out.println("responseData---"+responseData);
            ObjectMapper mapper = new ObjectMapper();
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
            targetConfigDto = mapper.readValue(responseData.getJSONObject("data").toString(), TargetConfigDto.class);
        }
            
        LOGGER.debug("responseEntity StatusCode {0}", responseEntity.getStatusCodeValue());
        return targetConfigDto;    
        
    }
	
	

}
