package com.tarang.middleware.smartrouter.service;

import com.tarang.middleware.smartrouter.dto.SourceMessageConfigResponse;
import com.tarang.middleware.smartrouter.dto.TargetConfigDto;

public interface MessageService {
    
    
    public SourceMessageConfigResponse getSourceMsgMapping(String port) throws Exception;
    
    public TargetConfigDto getTargetConfigByTargetName(String targetName) throws Exception;

}
