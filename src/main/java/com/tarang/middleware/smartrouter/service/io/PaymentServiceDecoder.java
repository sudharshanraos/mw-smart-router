package com.tarang.middleware.smartrouter.service.io;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.tomcat.util.buf.HexUtils;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.tarang.middleware.smartrouter.constants.Constant;
import com.tarang.middleware.smartrouter.dto.SourceMessageConfigResponse;
import com.tarang.middleware.smartrouter.service.MessageService;
import com.tarang.middleware.smartrouter.service.TargetPaymentHostService;
import com.tarang.middleware.smartrouter.service.iso.ISOMessageFactory;
import com.tarang.middleware.smartrouter.utils.ByteConversionUtils;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.bytes.ByteArrayDecoder;

/**
 * The Class PaymentServiceDecoder.
 * 
 * @author sudharshan.s
 */
@Sharable
@Service
public class PaymentServiceDecoder extends ByteArrayDecoder {

    /** The Constant log. */
    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentServiceDecoder.class);

    /** The message factory. */
    @Autowired
    private ISOMessageFactory messageFactory;
    
    @Autowired
    private TargetPaymentHostService targetPaymentHostService;
    
    @Autowired
    private MessageService messageService;

    /** The hitting live server. */
    @Value("${paymentservice.hittingLiveServer}")
    private boolean hittingLiveServer;

    /** The header length. */
    private int headerLength = 5;
    
    private HttpHeaders sourceHeaders;
    
    @Value("${target.server.host.url}")
    private String targetServerHostUrl;
    
    @PostConstruct
    public void postConstruct() {

        sourceHeaders = new HttpHeaders();
        sourceHeaders.setContentType(MediaType.APPLICATION_JSON);
        
    }
    
    /**
     * Decode.
     *
     * @param ctx
     *            the ctx
     * @param in
     *            the in
     * @param out
     *            the out
     * @throws Exception
     *             the exception
     */
    @Override
    public void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        try {
            if (in.isReadable()) {
                byte[] responseMessage = new byte[in.readableBytes()];
                in.getBytes(0, responseMessage);
                LOGGER.info("Original Message from host : {} ", responseMessage.toString());
                LOGGER.info("Get Response from host : {} ", HexUtils.toHexString(responseMessage));
                LOGGER.info("Get hex request from host : {} ", ByteConversionUtils.byteArrayToString(responseMessage, responseMessage.length));
                // Get hex request
            //   String hexReq = ByteConversionUtils.byteArrayToHexString(responseMessage, responseMessage.length, false);
                
                String hexReq = ByteConversionUtils.byteArrayToString(responseMessage, responseMessage.length);
                // Get length of request
                int reqLen = Integer.parseInt(hexReq.substring(0, 4), 16);
               // int reqLen = hexReq.length();
                //int reqLen = Integer.parseInt(hexReq.substring(0, hexReq.length()), 16);
                byte[] afterBuffer;
                if (hittingLiveServer) {
                    reqLen = reqLen - headerLength;
                    afterBuffer = ISOUtil.hex2byte(hexReq.substring(14, hexReq.length()).trim().getBytes(), 0, reqLen);
                } else {
                	afterBuffer = ISOUtil.hex2byte(hexReq.substring(4, hexReq.length()).trim().getBytes(), 0, reqLen);
                    //afterBuffer = ISOUtil.hex2byte(hexReq.substring(0, hexReq.length()).trim().getBytes(), 0, reqLen);
               	    //afterBuffer = ISOUtil.hex2byte(hexReq.trim().getBytes(), 0, reqLen);
                    //afterBuffer = hexReq.getBytes();
                }

                SourceMessageConfigResponse sourceConfigResponse = messageService.getSourceMsgMapping(Constant.MERCHANT_CONFIG_HOST_PORT);

                
                ISOMsg isoMessage = new ISOMsg();
                if (reqLen == afterBuffer.length) {
                    isoMessage = messageFactory.unpackISOFormat(afterBuffer,sourceConfigResponse.sourceObj.getIsoPackager());
                    logISOMsg(isoMessage);
                 //  
                } else {
                    LOGGER.info("Receive the packet length not match...");
                }
                in.clear();
                out.add(isoMessage);
               
                targetPaymentHostService.sendHost(isoMessage,sourceConfigResponse.sourceObj.getDefaultTarget());
            
            }
        } catch (Exception e) {
            LOGGER.error("Unexpected exception while decoding  : {}", e);
            throw new IllegalArgumentException("unexpected exception", e);
        }
    }

    
   
    
    
    public static void logISOMsg(ISOMsg msg) {
        System.out.println("----ISO MESSAGE-----");
        try {
            System.out.println("  MTI : " + msg.getMTI());
            for (int i=1;i<=msg.getMaxField();i++) {
                if (msg.hasField(i)) {
                    System.out.println("    Field-"+i+" : "+msg.getString(i));
                }
            }
        } catch (ISOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("--------------------");
        }
 
    }
}
