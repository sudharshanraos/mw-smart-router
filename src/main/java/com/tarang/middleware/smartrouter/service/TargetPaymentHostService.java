package com.tarang.middleware.smartrouter.service;

import org.jpos.iso.ISOMsg;

public interface TargetPaymentHostService {
	
	 public ISOMsg  sendHost( ISOMsg isoMessage, String targetName) throws Exception;

}
