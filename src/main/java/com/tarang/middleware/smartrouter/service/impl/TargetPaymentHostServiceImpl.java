package com.tarang.middleware.smartrouter.service.impl;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.tarang.middleware.smartrouter.dto.ISOBaseMessage;
import com.tarang.middleware.smartrouter.dto.TargetConfigDto;
import com.tarang.middleware.smartrouter.dto.TransactionRequest;
import com.tarang.middleware.smartrouter.service.MessageService;
import com.tarang.middleware.smartrouter.service.TargetPaymentHostService;
import com.tarang.middleware.smartrouter.service.iso.ISOMessageFactory;

@Service
public class TargetPaymentHostServiceImpl implements TargetPaymentHostService {

	/** The message factory. */
	@Autowired
	private ISOMessageFactory messageFactory;

	@Autowired
	private MessageService messageService;
	
	@Value("${target.server.host.url}")
	private String targetServerHostUrl;
	
	public ISOMsg sendHost(ISOMsg isoMessage, String targetName) throws Exception {
		System.out.println("Enter into sendHost");
		ISOMsg responseMessage = null;
		try {
			TargetConfigDto targetConfigDto = messageService.getTargetConfigByTargetName(targetName);
			RestTemplate restTemplate = new RestTemplate();
			ISOBaseMessage posRequest = messageFactory.parsePosGatewayRequest(isoMessage);
			// String targetRequest = prepareTargetRequest(isoMessage, posRequest,
			// packagerXml);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			TransactionRequest req = new TransactionRequest();
			//req.setHost("88.213.84.66"); // TODO : TargetHost from db
			req.setHost(targetConfigDto.getHost());
			//req.setPort("2008"); // TODO : TargetPort from DB
			req.setPort(targetConfigDto.getPort());
			req.setMessage(posRequest);
			//req.setPackagerPath(null);// TODO :TargetPort from DB --
			req.setPackagerPath(targetConfigDto.getIsoPackager());
			req.setTpduHeader(null);
			HttpEntity<ISOBaseMessage> entity = new HttpEntity<ISOBaseMessage>(posRequest, headers);
			// Need to mention the mada processor url
			ResponseEntity<String> responseEntity = restTemplate.postForEntity(targetServerHostUrl, req, String.class);
			String gatewayResponse = responseEntity.getBody();
			System.out.println("Response from Mada Simulator " + gatewayResponse);
			//responseMessage = prepareAuthResponse(isoMessage, posRequest, gatewayResponse);

			//System.out.println("responseMessage----" + responseMessage.getMTI().toString());
		} catch (ISOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return responseMessage;

	}

	public ISOMsg prepareAuthResponse(ISOMsg requestMessage, ISOBaseMessage posRequest, String gatewayResponse) {
		ISOMsg responseISOMsg = new ISOMsg();

		try {
			responseISOMsg.setMTI("0210");
			responseISOMsg.set(3, requestMessage.getString(3));
			responseISOMsg.set(4, requestMessage.getString(4));
			responseISOMsg.set(11, requestMessage.getString(11));
			String txnTime = posRequest.getDateLocalTransaction13();
			responseISOMsg.set(12, txnTime);
			responseISOMsg.set(13, txnTime);
			responseISOMsg.set(24, "0001");
			responseISOMsg.set(37, "222222222222");
			responseISOMsg.set(38, "333300");
			responseISOMsg.set(39, "00");
			responseISOMsg.set(41, posRequest.getTermTypeData127_25_23());
			// responseISOMsg.set(42, "102000000005197");

		} catch (ISOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return responseISOMsg;
	}

}
