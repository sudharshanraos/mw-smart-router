package com.tarang.middleware.smartrouter.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tarang.middleware.smartrouter.constants.ApiConstants;
import com.tarang.middleware.smartrouter.dto.ISOBaseMessage;
import com.tarang.middleware.smartrouter.dto.PosRequest;
import com.tarang.middleware.smartrouter.dto.TransactionResponse;
import com.tarang.middleware.smartrouter.enums.TransactionTypes;
import com.tarang.middleware.smartrouter.service.io.PaymentHostService;
import com.tarang.middleware.smartrouter.service.iso.ISOMessageFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
//@RequestMapping(ApiConstants.PS_MADA_PARENT)
@Api(value = ApiConstants.SOFT_POS_SERVICE_MANAGEMENT, tags = {
        ApiConstants.SOFT_POS_SERVICE_MANAGEMENT
})
public class SoftPOSController {
	
	  /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SoftPOSController.class);

    /** The message factory. */
    @Autowired
    private ISOMessageFactory messageFactory;

    /** The payment host service. */
    @Autowired
    private PaymentHostService paymentHostService;

    /**
     * Auth.
     *
     * @param request
     *            the request
     * @return the transaction response
     */
    @PostMapping(value = ApiConstants.SOFT_POS_MADA_AUTH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Process the auth transaction")
    @ResponseStatus(HttpStatus.OK)
    public TransactionResponse posAuthReq(@RequestBody PosRequest request) {
        LOGGER.info("Process the auth transaction");
        try {
        	ISOBaseMessage isoBaseRequest = messageFactory.getISOBasePacket(request); //Need to convert JSON request to ISOBaseRequest
            paymentHostService.send(messageFactory.createISOMessage(isoBaseRequest, TransactionTypes.AUTH), request.getHost(),
                    request.getPort());
        } catch (Exception e) {
            LOGGER.error("ERROR: process the auth transaction - {}", e);
        }
        return new TransactionResponse();
    }

}
