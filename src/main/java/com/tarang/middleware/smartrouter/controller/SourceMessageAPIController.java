package com.tarang.middleware.smartrouter.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tarang.middleware.smartrouter.constants.ApiConstants;
import com.tarang.middleware.smartrouter.dto.SourceMessageConfigResponse;
import com.tarang.middleware.smartrouter.service.MessageService;
import com.tarang.middleware.smartrouter.utils.GeneralResponseCreator;

@RestController
@RequestMapping(ApiConstants.SOURCE_MSG_MAPPING)
/*
 * @Api(value = NamespaceConstants.SOURCE_MSG_MAPPING_MANAGEMENT, tags = {
 * NamespaceConstants.SOURCE_MSG_MAPPING_MANAGEMENT })
 */
public class SourceMessageAPIController<T> {
    
    private static final Logger log = LoggerFactory.getLogger(SourceMessageAPIController.class);
    
    @Autowired
    protected MessageService msgService;
    
    /** The response creator. */
    @Autowired
    private GeneralResponseCreator responseCreator;
    
    @RequestMapping(path = ApiConstants.GET_SOURCE_MSG_MAPPING, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
   // @ResponseStatus(HttpStatus.OK)
    //@RequestMapping(path = ApiConstants.GET_SOURCE_MSG_DETAILS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public SourceMessageConfigResponse getSourceMsgMaggingDetails(String port) throws Exception {
        log.info("-------SourceMessageAPIController /getSourceMsgMaggingDetails()--------- Started");
        SourceMessageConfigResponse details = msgService.getSourceMsgMapping(port);
        log.info("-------SourceMessageAPIController /getSourceMsgMaggingDetails()--------- end ");
        //return responseCreator.createGeneralResponse(ErrorCodes.SUCCESS, true, (T) details);
        return details;
    }

}
