/*
 * 
 */
package com.tarang.middleware.smartrouter.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The Class TargetConfigDto.
 * 
 * @author sudharshan.s
 */
@Getter
@Setter
@NoArgsConstructor
public class TargetConfigDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	private long id;

	/** The target name. */
	private String targetName;

	/** The merchant id. */
	private String merchantId;

	/** The connection type. */
	private String connectionType;

	/** The message format. */
	private String messageFormat;

	/** The uri. */
	private String uri;

	/** The headers. */
	private String headers;

	/** The port. */
	private String port;

	/** The sign in. */
	private String signIn;

	/** The schema type. */
	private String schemaType;

	/** The encrypt key. */
	private String encryptKey;

	/** The encrypt type. */
	private String encryptType;

	/** The decrypt key. */
	private String decryptKey;

	/** The decrypt type. */
	private String decryptType;

	/** The heartbeat. */
	private String heartbeat;

	/** The heartbeat dealy. */
	private String heartbeatDealy;

	/** The retry. */
	private String retry;

	/** The retry count. */
	private String retryCount;

	/** The connection timeout. */
	private String connectionTimeout;

	/** The Request timeout. */
	private String RequestTimeout;
	
	
	/** The created by.*/
	private String createdBy;
	
	
	/** The created Date.*/
	private Date createdDate;
	
	
	/** The  updated by.*/
	private String updatedBy;
	
	
	/** The  updated date.*/ 
	private Date updatedDate;
	
	private String host;
	
	private String isoPackager;
	
	

}
