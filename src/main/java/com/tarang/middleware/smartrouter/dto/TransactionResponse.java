package com.tarang.middleware.smartrouter.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The Class TransactionResponse.
 * 
 * @author sudharshan.s
 */
@Getter
@Setter
@NoArgsConstructor
public class TransactionResponse implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The message. */
    private ISOBaseMessage message;

}
