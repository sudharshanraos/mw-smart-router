package com.tarang.middleware.smartrouter.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PosRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String txnType;
	private String paymentMode;
	private String dateAndTime;
	private String merchantId;
	private String terminalId;
	private String orderId;
	private String stan;
	private BigDecimal amount;
	private String currency;
	private String cardNumber;
	private String cardExpiryMonth;
	private String cardExpiryYear;
	private String cardCvv;
	private String cardHolderName;
	private String statementNarrative;
	private String pgTxnId;
	private String reqType;
	private String PIN;
	private String processingCode;
	private String mti;
	private String port;
	private String host;
	

}
