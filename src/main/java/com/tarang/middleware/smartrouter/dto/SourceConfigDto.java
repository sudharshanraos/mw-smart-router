package com.tarang.middleware.smartrouter.dto;

import lombok.Getter;

@Getter
public class SourceConfigDto  {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    private Long id;

    /** The source name. */
    private String sourceName;

    /** The merchant id. */
    private String merchantId;

    /** The connection type. */
    private String connectionType;

    /** The message format. */
    private String messageFormat;

    /** The uri. */
    private String uri;

    /** The headers. */
    private String headers;

    /** The port. */
    private String port;

    /** The available targets. */
    private String availableTargets;

    /** The default target. */
    private String defaultTarget;

    /** The encrypt key. */
    private String encryptKey;

    /** The encrypt type. */
    private String encryptType;

    /** The decrypt key. */
    private String decryptKey;

    /** The decrypt type. */
    private String decryptType;

    /** The validation. */
    private String validation;

    /** The transaction log. */
    private String transactionLog;

    /** The validation check. */
    private String validationCheck;

    /** The connection timeout. */
    private String connectionTimeout;

    /** The Request timeout. */
    private String RequestTimeout;
    
    private String isoPackager;
}
