package com.tarang.middleware.smartrouter.dto;

import lombok.Getter;

@Getter
public class MessageConfig {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    private Long id;

    /** The transaction type. */
    private String transactionType;

    /** The iso request. */
    private String isoRequest;

    /** The iso response. */
    private String isoResponse;

    /** The xml request. */
    private String xmlRequest;

    /** The xml response. */
    private String xmlResponse;

    /** The json request. */
    private String jsonRequest;

    /** The json response. */
    private String jsonResponse;

}
