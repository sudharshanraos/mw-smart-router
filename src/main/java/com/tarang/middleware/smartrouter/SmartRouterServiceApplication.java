package com.tarang.middleware.smartrouter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * The Class SmartRouterServiceApplication.
 * 
 * @author sudharshan.s
 */
@EnableScheduling
@SpringBootApplication
public class SmartRouterServiceApplication {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SmartRouterServiceApplication.class);

    /**
     * The main method.
     *
     * @param args
     *            the arguments
     */
    public static void main(String[] args) {
        LOGGER.info("SmartRouterServiceApplication Starting..");
        ConfigurableApplicationContext context = SpringApplication.run(SmartRouterServiceApplication.class, args);
     //  SpringApplication.run(SmartRouterServiceApplication.class, args);
      
        PosMessageServer isoMessagesServer = context.getBean(PosMessageServer.class);
        isoMessagesServer.start();
       
    }
    
}
